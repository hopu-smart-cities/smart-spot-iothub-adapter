echo mqttUrl="'mosquitto'" > .env
echo mqttUser="'libelium'" >> .env
echo mqttPassword="'libelium'" >> .env
echo mqttPort=1883 >> .env
echo mqttProtocol="'mqtt'" >> .env
echo rejectUnauthorized=false >> .env
echo targetTopic="'/123456789abcdefghijklmnop/#'" >> .env
echo azureConnectionString="'yourconnectionstringhere'" >> .env
echo log_level="'info'" >> .env
mkdir logs