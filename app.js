const mqtt = require('mqtt');
require('dotenv').config()

const logger = require('./modules/logger')
const azure = require('./modules/azure_connector');


const brokerUrl = process.env.mqttProtocol +'://'+ process.env.mqttUrl;



// Connect options
const connectOptions = {
  clientId: 'mqtt2iothub-client',
  username: process.env.mqttUser,
  password: process.env.mqttPassword,
  protocolId: 'MQTT',
  protocolVersion: 4,
  port: process.env.mqttPort, 
  rejectUnauthorized: Boolean(process.env.rejectUnauthorized),
  keepalive: 60, 
  reconnectPeriod: 1000, 
  clean: true 
  
};

// Create an MQTT client instance
const client = mqtt.connect( brokerUrl, connectOptions );


// Connection event
client.on('connect', () => {
  logger.info('Connected to MQTT broker');
  // You can subscribe to topics or perform other operations here
  let targetTopic = process.env.targetTopic

  // Subscribe to a topic
  client.subscribe(targetTopic, { qos: 0 }, (error) => {
    if (error) {
      logger.error('Error subscribing:', error);
      // Handle subscription error here
    } else {
      logger.info('Subscribed to topic');
    }
  });
});

// Error event
client.on('error', (error) => {
  logger.error('Error:', error);
  // Handle connection errors here
});

const topic_regex = new RegExp('\/([0-9a-zA-Z]{25})\/([0-9a-zA-Z_]+)\/attrs')

// Message event
client.on('message', (topic, message) => {
  message = JSON.parse(message.toString())
  logger.debug('On topic: ', topic, 'Received: ', message);

  let match = topic.match(topic_regex)
  
  if(match){
    
    logger.debug("Received message from: ",match[2])

    try{
        azure.sendMessage(JSON.stringify({

            "device":match[2],
            "message":message
        }))
    }catch(e){
        logger.warn("Adapter is not connected to Azure, cannot forward message")
        logger.error(e)
    }
  }

});


// Disconnect event
client.on('disconnect', () => {
  logger.error('Disconnected from MQTT broker');
});
