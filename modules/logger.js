const winston = require('winston')
const { format } = require('winston');


const logger = winston.createLogger({ level: process.env.log_level, // error: 0, warn: 1, info: 2, http: 3, verbose: 4, debug: 5, silly: 6
    format: format.combine(
        format.timestamp(),
        format.printf(({ level, message, timestamp }) => {
          return `time=${timestamp} | lvl=${level} | comp=MQTT2IoTHub | msg=${message}`;
        })
      ), // Log format
    transports: [
      new winston.transports.Console(), // Output logs to the console
      new winston.transports.File({ filename: './logs/app.log' }) // Output logs to a file
    ]
  });

module.exports = logger;
  