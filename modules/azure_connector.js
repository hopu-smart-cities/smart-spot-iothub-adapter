require('dotenv').config();

const Client = require('azure-iot-device').Client;
const Message = require('azure-iot-device').Message;
const MqttProtocol = require('azure-iot-device-mqtt').Mqtt;

const logger = require('./logger')

const connectionString = process.env.azureConnectionString
if(!connectionString.includes("HostName")||!connectionString.includes("DeviceId")||!connectionString.includes("SharedAccessKey")){
    logger.error(`Invalid iothub connection string: ${connectionString}`)
    return;
}

const client = Client.fromConnectionString(connectionString, MqttProtocol);

var isMessageSendOn = false;

client.on('message', (msg) => {
    logger.debug('Received message: ',msg.getData().toString());
  
    // Complete the message to acknowledge receipt
    client.complete(msg, (err) => {
      if (err) {
        logger.error('Error completing message:', err.toString());
      } else {
        logger.debug('Message completed.');
      }
    });
  });

client.open((err) => {

    if (err) {
        logger.error('[IoT Hub Client] Connect error:\n\t' + err.message);
        return;
    }
    logger.info("Connected to IoTHub")
    isMessageSendOn = true;

    client.onDeviceMethod('start', (request, response) => {

        response.send(200, 'Successully connecting starting message conversion to cloud', function(err) {
            if (err) {
                logger.error('[IoT Hub Client] Failed sending a start method response due to:\n\t' + err.message);
            }
        });

    });

});

function sendMessage(content) {
    if (!isMessageSendOn) { return; }

    var message = new Message(content.toString('utf-8'));
    message.contentEncoding = 'utf-8';
    message.contentType = 'application/json';

    client.sendEvent(message, (err) => {
        if (err) {
            logger.error('[AzureAdapter] Failed to send message to Azure IoT Hub due to:\n\t' + err.message);
        } else {
            logger.debug('[AzureAdapter] Message sent to Azure IoT Hub');
        }
    });
}

module.exports = {sendMessage}