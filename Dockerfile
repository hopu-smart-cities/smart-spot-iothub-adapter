FROM node:19

WORKDIR /src/source

COPY . .

RUN npm install

CMD ["node","app.js"]