# MQTT TO AZURE IOT HUB ADAPTER

This repository contains the necessary scripts to integrate a fiware-ready device like smartspot to iothub. 

## Requirements

- docker
- docker-compose

## Configuration

For the program to start it needs a file which will contain all the necessary variables for it to work, that file is the ".env" file, to generate the variables file, execute the following command:

```shell
bash auxScripts/generateConfigFile.sh
```

Now you may modify the [variable file](.env)

The variables are the following: 

| ENV                   | Default                     | Description                                                                        | Options                                 |
|-----------------------|-----------------------------|------------------------------------------------------------------------------------|-----------------------------------------|
| mqttUrl               | mosquitto                   | URL to the MQTT broker where the Smartspot is publishing                           |                                         |
| mqttUser              | libelium                    | MQTT username                                                                      |                                         |
| mqttPassword          | libelium                    | MQTT password                                                                      |                                         |
| mqttPort              | 1883                        | MQTT port                                                                          | 1883, 8883                              |
| mqttProtocol          | mqtt                        | The MQTT protocol to be used, will create final hostname                           | mqtt, mqtts                             |
| rejectUnauthorized    | false                       | Variable to establish in case of TLS based broker                                  | true, false                             |
| targetTopic           | /123456789abcdfghijklmnop/# | The API key that the devices use to report on, must have the structure /{apikey}/# |                                         |
| azureConnectionString | yourconnectionstringhere    | Azure's connection string needed to report on the iothub                           |                                         |
| log_level             | info                        | Logger level of info                                                               | error, warn, info, http, verbose, debug |


## Flow 

```mermaid
flowchart LR
    origin((SmartSpot Device)):::garnet
    target[(Azure IoTHub)]:::garnet
    
    origin-->mqtt_broker[MQTT Broker]
    mqtt_broker-->adapter(IoTHub adapter)
    adapter-->target
    
    classDef garnet fill:#721124,color:#fff,stroke:#fff
```

## Execution

After modifying the .env file you may now start deploying the service, [app.js](app.js) starts the service

### Docker

#### Build

```shell
docker build -t local/iothub-adapter .
```

#### Initialization

- Start docker swarm

```shell
docker swarm init
```

- Create the network where it will work 

```shell
docker network create --driver overlay --attachable adapter-network
```

#### Deploy

```shell
docker stack deploy -c docker-compose/docker-compose.yml iothub
```

Now your program is fully running

#### Log

```shell
docker service logs --follow iothub_adapter
```
If everything is running correctly you should see a message like this ![connection-success](img/connection-success.png)

##### Example

You can use the configuration given by default in the [.env](../.env) file except for the "azureConnectionString" variable, the program will connect to a mosquitto container deployed with the adapter,
then will send all frames that comes through a topic like "/123456789abcdefghijklmnop/Device_AQWS_01/attrs" to the iothub 

#### Stop

```shell
docker stack rm iothub
``` 